---
title: "Products"
---
# September - Subdivision Utilities [User Guide](September_User_Guide.pdf)

{{< figure src="September.png" title="September : Limit Evaluation at UV Locations" width=75% height=auto >}}

## Downloads

### Windows
- [Windows 10 - Houdini 20.5](September-Windows-10-AMD64-H20.5-1.1.0-win64.zip)


### Linux

- [Rocky 8 - Houdini 19.5](September-rocky-8-x86_64-H19.5-1.1.0-Linux.zip)
- [Rocky 8 - Houdini 20.0](September-rocky-8-x86_64-H20.0-1.1.0-Linux.zip)
- [Rocky 8 - Houdini 20.5](September-rocky-8-x86_64-H20.5-1.1.0-Linux.zip)

### MacOS

- [MacOS 11 Intel - Houdini 19.5](September-MacOS-11-x86_64-H19.5-1.1.0-Darwin.zip)
- [MacOS 11 Intel - Houdini 20.0](September-MacOS-11-x86_64-H20.0-1.1.0-Darwin.zip)
- [MacOS 11 Intel - Houdini 20.5](September-MacOS-11-x86_64-H20.5-1.1.0-Darwin.zip)
