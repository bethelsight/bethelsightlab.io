---
title: "Downloads"
---
## Available Downloads
### Geometry Tools [User Guide](USERGUIDE.pdf)

#### Linux
[0.1.0 Linux-AppImage](GeometryTools-x86_64.AppImage)

#### Windows
[0.1.0 Windows MSI](geometrytools-0.1.0-windows-amd64.exe)

#### Glance integration
{{< figure src="Suzanne_euclidean.png" title="GeometryTools output loaded in Glance" width=75% height=auto >}}
