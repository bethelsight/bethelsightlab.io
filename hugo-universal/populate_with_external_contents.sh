#!/bin/sh

# This is a post processing script not for general use

cp ~/September_User_Guide.pdf ../../public_html_staging/products/
cp ~/projects/september/project-september_git/docs/September.png ../../public_html_staging/products/

# Windows download
cp ~/Downloads/September-Windows-10-AMD64-H20.5-1.1.0-win64.zip ../../public_html_staging/products/
# Linux download
cp ~/Downloads/September-rocky-8-x86_64-H19.5-1.1.0-Linux.zip ../../public_html_staging/products/
cp ~/Downloads/September-rocky-8-x86_64-H20.0-1.1.0-Linux.zip ../../public_html_staging/products/
cp ~/Downloads/September-rocky-8-x86_64-H20.5-1.1.0-Linux.zip ../../public_html_staging/products/
# MacOS dowwnload
cp ~/Downloads/September-MacOS-11-x86_64-H19.5-1.1.0-Darwin.zip ../../public_html_staging/produ\
cts/
cp ~/Downloads/September-MacOS-11-x86_64-H20.0-1.1.0-Darwin.zip ../../public_html_staging/produ\
cts/
cp ~/Downloads/September-MacOS-11-x86_64-H20.5-1.1.0-Darwin.zip ../../public_html_staging/produ\
cts/
