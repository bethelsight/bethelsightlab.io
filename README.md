# bethelsight.gitlab.io

Hugo content for bethelsight.ca

Currently using hugo-universal directory as source of website

## Local server testing
```
hugo server
```

## Publish to directory for uploading
```
hugo --destination ../../public_html_staging
```

## Test published site
```
pushd ../public_html_staging
python3 -m http.server
```
